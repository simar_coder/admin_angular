export interface User {

    id: string;
    _id: string;
    banners: [];
    isActivated: boolean;
    isSuccess: boolean;
    userId: string;
    firstName: string;
    lastName: string;
    addressLine1: string;
    addressLine2: string;
    emailId: string;
    about: string;
    fullName: string;
    email: string;
    type: string;
    mobile: number;
    phoneNumber: string;
    role: string;
    city: string;
    confirmPassword: string;
    password: string;
    newPassword: string;
    token: string;
    createdAt: string;
    otp: string;
    otpToken: string;
    avatarImages: string;
    category: string;
    description: string;
    country: string;
    zipCode: string;
    lat: string;
    long: string;
    answer: string;
    securityQuestion: string;
    status: string;

}
