import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatMenuModule} from '@angular/material/menu';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { UserListComponent } from './user-list/user-list.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material/core';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserStatsComponent } from './user-stats/user-stats.component';

@NgModule({
  declarations: [
    DashboardComponent,
    UserListComponent,
    UserProfileComponent,
    UserStatsComponent
  ],
  imports: [
    CommonModule,
    MatSidenavModule, MatIconModule, MatToolbarModule,MatListModule,
    DashboardRoutingModule,    
    FormsModule,
    HttpClientModule,
    MatMenuModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    CommonModule 
  ]
})
export class DashboardModule { }
