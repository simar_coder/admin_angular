import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ApiServiceService } from '../../shared/api-service.service';
import { LocalStorageService } from '../../shared/local-storage.service';
import { User } from '../../shared/models/user';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  isLoading: boolean = false;
  usersData: any = {};
  userList: any = [];
  temp: any = [];
  submitted: any;

  // for table 

  constructor(
    private router: Router, private apiservice: ApiServiceService,
  ) { }

  ngOnInit(): void {
    this.getUsersList();
  }

  // user List
  getUsersList() {
    this.isLoading = true;
    this.apiservice.usersList().subscribe(res => {
      this.userList = res;
    });
  }
  deleteUser(user: any) {
    debugger;
    var addRemove: any = {
      id: user._id,
    }
    this.apiservice.deleteUsers(addRemove).subscribe(res => {
        if (this.usersData.statusCode === 200)
      {
        return this.getUsersList();
      }
    });

  }
}
