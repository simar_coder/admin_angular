import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
// import { MatProgressBar, MatButton } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ApiServiceService } from '../../shared/api-service.service';
import { LocalStorageService } from '../../shared/local-storage.service';
import { User } from '../../shared/models/user';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  changePasswordForm: any = FormGroup;
  isLoading: boolean = false;
  userData: any = {};
  constructor(
    private router: Router, private apiservice: ApiServiceService,
  ) { }

  ngOnInit(): void {
    this.changePasswordForm = new FormGroup({
      oldPassword: new FormControl('', Validators.required),
      newPassword: new FormControl('', Validators.required),
    });
  }
  changePassword() {
    debugger;
      this.isLoading = true
      debugger;
      this.apiservice.changePass(this.changePasswordForm.value).subscribe(res => {
        this.userData = res;
        console.log('change password', this.userData)
        if (this.userData.statusCode === 200)
         {
          this.router.navigate(['dashboard/user']);
      // this.router.navigateByUrl('')
          this.isLoading = false;
        } 
  
        else {
          alert('Wrong username or password!');
          // return this.isLoading = false;
        };
      });
    
  
  }
}
